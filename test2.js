var request = require("request");
require('request-debug')(request);
var cheerio = require('cheerio');
const fs = require('fs');
const path = require('path');

//Permitimos acceso con certificado firmado por ellos mismos
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

//Variables globales

var crsf; //variable global con token csrf

//carga pagina inicial
var options = { method: 'GET',
url: 'https://webpol.policia.es/e-hotel/',
jar: true,
headers: 
 { 'cache-control': 'no-cache',
   //cookie: 'sto-id-60565-38380_appoperativas=CMAIBKKMOMJF; path=/; HttpOnly=; sto-id-60565-38380_appoperativas\tCNAIBKKMOMJF=; FRONTAL_JSESSIONID=EOBtH3K1VIZNN33C_9t244oXamTjC8UJM0KfjZYWv881Uu4RKaZs!-1949212582; ; FRONTAL_JSESSIONID=HMZtQs26mjoD8qGmABGryqNpUYfa68suIF2mF2w33ffKoiv8-X8z!1067256408',
   'accept-language': 'en-US,en;q=0.8,es;q=0.6',
   'accept-encoding': 'gzip, deflate, br',
   referer: 'https://www.google.es/',
   accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
   'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
   'upgrade-insecure-requests': '1' } };

request(options, function (error, response, body) {
    if (error) throw new Error(error);
    var $ = cheerio.load(body);
    csrf = $('input[name="' + "_csrf" + '"]').val();
    console.log("CRSF token: "+csrf);
//Aqui se podria implementar la captura de la cookie y mostrar JSESSION_ID    
    login('H28391AAV2E','Cascorro175c');
//console.log(body);
});

//Funcion que realiza el login
function login(user, pass){
var options = { method: 'POST',
url: 'https://webpol.policia.es/e-hotel/execute_login',
followAllRedirects: true,
jar: true,
headers: 
 { 'cache-control': 'no-cache',
   //cookie: 'FRONTAL_JSESSIONID=-FJtJIvLytPIjDTrP8RPXpbugWpLQ5VyDLGxRTPfQCCLHlR6t5J1!1067256408; path=/; HttpOnly',
   'accept-language': 'en-US,en;q=0.8,es;q=0.6',
   'accept-encoding': 'gzip, deflate, br',
   referer: 'https://webpol.policia.es/e-hotel/',
   accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
   'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
   'content-type': 'application/x-www-form-urlencoded',
   'upgrade-insecure-requests': '1',
   origin: 'https://webpol.policia.es' },
  form:
  { _csrf: csrf,
    password: pass,
    username: user
  } };

request(options, function (error, response, body) {
  //console.log(body);
  if (error) throw new Error(error);
  subirArchivo();
  //console.log(body);
});
}

function subirArchivo(){

    //Preparamos los datos del formulario
    var data = { fichero: 
        { value: fs.createReadStream(path.normalize(__dirname+'/28391AAV2E.001')),
          options: { filename: '28391AAV2E.001', contentType: 'application/octet-stream' } },
       jsonHiddenComunes: '',
       _csrf: csrf };

    //preparamos las opciones de la llamada
    var options = { method: 'POST',
    url: 'https://webpol.policia.es/e-hotel/hospederia/pruebas/subirFichero',
    formData: data,
    jar: true,
    headers: 
     { 'cache-control': 'no-cache',
       'accept-language': 'en-US,en;q=0.8,es;q=0.6',
       'accept-encoding': 'gzip, deflate, br',
       referer: 'https://webpol.policia.es/e-hotel/inicio',
       'ajax-referer': 'hospederia/pruebas/subirFichero',
       'x-requested-with': 'XMLHttpRequest',
       'X-CSRF-TOKEN': csrf,       
       'x-devtools-emulate-network-conditions-client-id': 'bfc71b5a-8f61-411b-99dd-6054aecdee75',
        accept: 'application/json, text/javascript, */*; q=0.01',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
        origin: 'https://webpol.policia.es' },
    };

    //llamamos la subida del archivo
    request(options, function (error, response, body) {
        if (error) throw new Error(error);
      
        console.log(body);
      });
}
